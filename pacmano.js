document.addEventListener('DOMContentLoaded', () => {
    const canvas = document.getElementById('pacman-map');
    const ctx = canvas.getContext('2d');
    const mapButton = document.querySelector('.btn');
    const audioMorreu = new Audio('morreu.m4a')
    // const audioIntro = new Audio('intro.wav');
    
    // document.addEventListener('click', () => {
    //     audioIntro.play();
    // }, { once: true });

    mapButton.addEventListener('click', function() {
        location.reload();
        });



    // Define the map using a matrix
    maps = {
        mapUm:[
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
            ],
        mapDois:[
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
            [1, 0, 1, 1, 1, 0, 1, 1, 0, 1],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
            [1, 0, 1, 0, 1, 0, 1, 0, 1, 1],
            [1, 0, 0, 0, 1, 0, 0, 0, 0, 1],
            [1, 0, 1, 0, 1, 0, 1, 1, 0, 1],
            [1, 0, 0, 0, 0, 0, 0, 1, 0, 1],
            [1, 0, 1, 0, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
            ],
        mapTres:[
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
            [1, 0, 1, 1, 0, 0, 1, 1, 0, 1],
            [1, 0, 0, 1, 0, 0, 1, 0, 0, 1],
            [1, 1, 0, 0, 0, 1, 0, 0, 1, 1],
            [1, 1, 0, 0, 0, 0, 0, 0, 1, 1],
            [1, 0, 0, 1, 0, 0, 1, 0, 0, 1],
            [1, 0, 1, 1, 0, 0, 1, 1, 0, 1],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
            ],
        mapQuatro:[
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
            [1, 0, 1, 1, 0, 1, 1, 1, 0, 1],
            [1, 0, 1, 1, 0, 1, 1, 1, 0, 1],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
            [1, 0, 1, 1, 0, 1, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 1, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 1, 1, 1, 0, 1],
            [1, 0, 1, 1, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
            ],
        mapCinco:[
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
            [1, 0, 1, 1, 1, 1, 1, 1, 0, 1],
            [1, 0, 0, 0, 1, 1, 0, 0, 0, 1],
            [1, 1, 1, 0, 1, 1, 0, 1, 1, 1],
            [1, 1, 1, 0, 0, 0, 0, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
            [1, 0, 1, 1, 0, 0, 1, 1, 0, 1],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
            ],
        mapSeis:[
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
            [1, 0, 1, 1, 0, 0, 1, 1, 0, 1],
            [1, 0, 1, 0, 0, 0, 0, 1, 0, 1],
            [1, 0, 1, 0, 0, 0, 0, 1, 0, 1],
            [1, 0, 1, 1, 1, 1, 1, 1, 0, 1],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
            [1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
            [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
            ],    
    }
    mapNames = [maps.mapUm,maps.mapDois,maps.mapTres,maps.mapQuatro,maps.mapCinco,maps.mapSeis]

    index = parseInt(Math.random()*6)

    map = mapNames[index]

    // Set the size of each tile in the map
    var tileSize = 60;

    // Loop through the map and draw each tile
    function settingColor(){
        for (var row = 0; row < map.length; row++) {
            for (var col = 0; col < map[row].length; col++) {
                // If the current tile is a wall (1), draw a blue square
                if (map[row][col] === 1) {
                    ctx.fillStyle = "#000000";
                    ctx.fillRect(col * tileSize, row * tileSize, tileSize, tileSize);
                    ctx.strokeStyle = "#0000FF";
                    ctx.lineWidth = 6;
                    ctx.strokeRect(col * tileSize, row * tileSize, tileSize, tileSize);
                    
                }
                // If the current tile is not a wall (0), draw a black circle with a yellow outline
                else {
                    ctx.fillStyle = "#000000";
                    ctx.fillRect(col * tileSize, row * tileSize, tileSize, tileSize);
                    ctx.fillStyle = "#FFFF00";
                    ctx.beginPath();
                    ctx.arc(col * tileSize + tileSize/2, row * tileSize + tileSize/2, tileSize/4, 0, Math.PI * 2);
                    ctx.fill();
                    ctx.strokeStyle = "#0000c9";
                    ctx.lineWidth = 2;
                    ctx.stroke();
                }
            }
        }
    }

    settingColor()
    fillTileWithImage(1,1,"red.png")


    canvas.addEventListener('click', function(event) {
        audioMorreu.play()
        const x = event.offsetX;
        const y = event.offsetY;
        const tileWidth = canvas.width / map[0].length;
        const tileHeight = canvas.height / map.length;
        const tileX = Math.floor(x / tileWidth);
        const tileY = Math.floor(y / tileHeight);
              
        // Call findShortestPath with the clicked tile position and log the result
        const shortestPath = findShortestPath(tileX, tileY);
        console.log(`O Pacmano está na posição ${[tileX,tileY]}`)
        // console.log("Caminhor mais curto: ")
        // console.log(shortestPath);
        colorTiles(shortestPath)
        //alert(shortestPath);
        
        // Change the color of the clicked tile to red
        
        ctx.fillStyle = "#000000";
        ctx.fillRect(tileX * tileWidth, tileY * tileHeight, tileWidth, tileHeight);
        ctx.fillStyle = "#FFFF00";
        ctx.beginPath();
        ctx.arc(tileX * tileWidth + tileWidth/2, tileY * tileHeight + tileHeight/2, tileWidth/2, Math.PI/4, 7*Math.PI/4);
        ctx.lineTo(tileX * tileWidth + tileWidth/2, tileY * tileHeight + tileHeight/2);
        ctx.closePath();
        ctx.fill();
        ctx.strokeStyle = "#0000c9";
        ctx.lineWidth = 2;
        ctx.stroke();
      });
      
      function colorTiles(tiles) {
        
        const tileWidth = canvas.width / map[0].length;
        const tileHeight = canvas.height / map.length;
      
        for (let i = 0; i < tiles.length; i++) {
          const tileX = tiles[i][0];
          const tileY = tiles[i][1];
          ctx.fillStyle = "#000000";
          ctx.fillRect(tileX * tileWidth, tileY * tileHeight, tileWidth, tileHeight);
          ctx.fillStyle = "#FF0000";
          ctx.beginPath();
          ctx.arc(tileX * tileWidth + tileWidth/2, tileY * tileHeight + tileHeight/2, tileWidth/8, 0, Math.PI * 2);
          ctx.fill();
          ctx.strokeStyle = "#0000c9";
          ctx.lineWidth = 2;
          ctx.stroke();
        }

        fillTileWithImage(tiles[tiles.length-2][0],tiles[tiles.length-2][1],"red.png")
        
      }
      
      
        
      function fillTileWithImage(x, y, imageSrc) {
        const tileWidth = canvas.width / map[0].length;
        const tileHeight = canvas.height / map.length;
      
        // Draw the image onto the canvas
        const img = new Image();
        img.src = imageSrc;
        img.onload = function() {
          ctx.drawImage(img, x * tileWidth, y * tileHeight, tileWidth, tileHeight);
        }
      
        // Update the matrix to reflect the image
        map[y][x] = "image";
      }
      

    function findShortestPath(clickedTileX, clickedTileY) {
        settingColor()
        // Cria a fila para guardar os vertices visitados
        const queue = [];
      
        // Marca o vertice inicial como visitado
        const visited = [[1, 1]];
        queue.push([1, 1, []]); // [x, y, caminho]
      
        // Definir as direçõs possiveis para se mover nas arestas
        const directions = [[-1, 0], [1, 0], [0, -1], [0, 1]]; // up, down, left, right
      
        while (queue.length > 0) {
          // Dequeue the next tile to visit
          const [x, y, path] = queue.shift();
      
          // Check if this tile is the clicked tile
          if (x === clickedTileX && y === clickedTileY) {
            // Return the shortest path to the clicked tile
            return path.concat([[x, y]]);
          }
      
          // Check each adjacent tile
          for (const [dx, dy] of directions) {
            const nextX = x + dx;
            const nextY = y + dy;
            
            // Ignore tiles outside the matrix or tiles that are walls (value 1)
            if (nextX < 0 || nextX >= map[0].length || nextY < 0 || nextY >= map.length || map[nextY][nextX] === 1) {
              continue;
            }
      
            // Check if this tile has already been visited
            const nextVisited = visited.some(([vx, vy]) => vx === nextX && vy === nextY);
            if (nextVisited) {
              continue;
            }
            
            // Mark the tile as visited and enqueue it
            visited.push([nextX, nextY]);
            
            queue.push([nextX, nextY, path.concat([[x, y]])]);

          }
          console.log(`Visitado ${visited}|`)
          console.log(`Fila ${queue}|`)
          console.log(`Caminho ${path}|`)
        }
        
        // If we've searched the entire matrix and haven't found the clicked tile, return null
        return null;
      }
      
});
  

